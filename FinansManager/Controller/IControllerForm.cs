﻿namespace FinansManager
{
	interface IControllerForm
	{
		void FillComboBoxSourceRevenue();
		void OpenFormSourceRevenues();
	}
}