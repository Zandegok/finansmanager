﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace FinansManager
{
	internal class ControllerFormMain : ControllerForm
	{
		private class DateSortRegulator
		{
			public RadioButton radioButtonAllTime;
			public RadioButton radioButtonOneDay;
			public RadioButton radioButtonPeriod;
			public Label labelAllTime;
			public Label labelOneDay;
			public Label labelPeriodFrom;
			public Label labelPeriodTo;
			public DateTimePicker dateTimePickerOneDay;
			public DateTimePicker dateTimePickerPeriodFrom;
			public DateTimePicker dateTimePickerPeriodTo;

			public DateSortRegulator(RadioButton radioButtonAllTime, RadioButton radioButtonOneDay, RadioButton radioButtonPeriod
				, Label labelAllTime, Label labelOneDay, Label labelPeriodFrom, Label labelPeriodTo
				, DateTimePicker dateTimePickerOneDay, DateTimePicker dateTimePickerPeriodFrom, DateTimePicker dateTimePickerPeriodTo)
			{
				this.radioButtonAllTime = radioButtonAllTime ?? throw new ArgumentNullException(nameof(radioButtonAllTime));
				this.radioButtonOneDay = radioButtonOneDay ?? throw new ArgumentNullException(nameof(radioButtonOneDay));
				this.radioButtonPeriod = radioButtonPeriod ?? throw new ArgumentNullException(nameof(radioButtonPeriod));
				this.labelAllTime = labelAllTime ?? throw new ArgumentNullException(nameof(labelAllTime));
				this.labelOneDay = labelOneDay ?? throw new ArgumentNullException(nameof(labelOneDay));
				this.labelPeriodFrom = labelPeriodFrom ?? throw new ArgumentNullException(nameof(labelPeriodFrom));
				this.labelPeriodTo = labelPeriodTo ?? throw new ArgumentNullException(nameof(labelPeriodTo));
				this.dateTimePickerOneDay = dateTimePickerOneDay ?? throw new ArgumentNullException(nameof(dateTimePickerOneDay));
				this.dateTimePickerPeriodFrom = dateTimePickerPeriodFrom ?? throw new ArgumentNullException(nameof(dateTimePickerPeriodFrom));
				this.dateTimePickerPeriodTo = dateTimePickerPeriodTo ?? throw new ArgumentNullException(nameof(dateTimePickerPeriodTo));

				this.radioButtonAllTime.CheckedChanged += RadioButton_CheckedChanged;
				this.radioButtonOneDay.CheckedChanged += RadioButton_CheckedChanged;
				this.radioButtonPeriod.CheckedChanged += RadioButton_CheckedChanged;
				RadioButton_CheckedChanged(null, null);

				dateTimePickerPeriodFrom.ValueChanged += DateTimePickerPeriodFrom_ValueChanged;
				dateTimePickerPeriodTo.ValueChanged += DateTimePickerPeriodTo_ValueChanged;
			}

			public void ResetDateTimePickers()
			{
				dateTimePickerOneDay.Value = DateTime.Now;
				dateTimePickerPeriodFrom.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
				dateTimePickerPeriodTo.Value = dateTimePickerPeriodFrom.Value.AddMonths(1).AddDays(-1);
			}

			private void DateTimePickerPeriodTo_ValueChanged(object sender, EventArgs e)
			{
				if (dateTimePickerPeriodFrom.Value > dateTimePickerPeriodTo.Value)
				{
					dateTimePickerPeriodFrom.Value = dateTimePickerPeriodTo.Value;
				}

			}

			private void DateTimePickerPeriodFrom_ValueChanged(object sender, EventArgs e)
			{
				if (dateTimePickerPeriodFrom.Value > dateTimePickerPeriodTo.Value)
				{
					dateTimePickerPeriodTo.Value = dateTimePickerPeriodFrom.Value;
				}
			}

			private void RadioButton_CheckedChanged(object sender, EventArgs e)
			{
				labelAllTime.Enabled = radioButtonAllTime.Checked;
				labelOneDay.Enabled = dateTimePickerOneDay.Enabled = radioButtonOneDay.Checked;
				labelPeriodFrom.Enabled = dateTimePickerPeriodFrom.Enabled = labelPeriodTo.Enabled = dateTimePickerPeriodTo.Enabled = radioButtonPeriod.Checked;
			}

		}

		private new FormMain form;
		private TextBoxStatisticsFiller textBoxStatisticsFiller;
		private DateSortRegulator dateSortRegulatorRevenue;
		private DateSortRegulator dateSortRegulatorExpences;
		private DateSortRegulator dateSortRegulatorStatistics;

		public ControllerFormMain(FormMain form) : base(form)
		{
			this.form = form;
			dateSortRegulatorRevenue = new DateSortRegulator(form.radioButtonRevenueAllTime, form.radioButtonRevenueOneDay, form.radioButtonRevenuePeriod,
				form.label22, form.label9, form.label20, form.label21,
				form.dateTimePickerRevenueOn, form.dateTimePickerRevenueFrom, form.dateTimePickerRevenueTo);
			dateSortRegulatorExpences = new DateSortRegulator(form.radioButtonExpencesAllTime, form.radioButtonExpencesOneDay, form.radioButtonExpencesPeriod,
				form.label23, form.label13, form.label24, form.label25,
				form.dateTimePickerExpencesOn, form.dateTimePickerExpencesFrom, form.dateTimePickerExpencesTo);
			dateSortRegulatorStatistics = new DateSortRegulator(form.radioButtonStatisticsAllTime, form.radioButtonStatisticsOneDay, form.radioButtonStatisticsPeriod,
				form.label6, form.label7, form.label8, form.label16,
				form.dateTimePickerStatisticsOn, form.dateTimePickerStatisticsFrom, form.dateTimePickerStatisticsTo);
		}

		public void ControlsInitialize()
		{
			DefaultingDateTimePickers();
			form.dateTimePickerExpencesTo.Value = form.dateTimePickerRevenueTo.Value = form.dateTimePickerStatisticsTo.Value = DateTime.Now.Date.AddMonths(1);
			FillData();
			FillBalanceFills();
			form.numericUpDownRevenueSumm.Maximum = decimal.MaxValue;
			form.numericUpDownExpencesSumm.Maximum = decimal.MaxValue;
		}

		public void DbManagerRandomFill()
		{
			DateTime start = form.dateTimePickerBalanceFrom.Value;
			DateTime finish = form.dateTimePickerBalanceTo.Value;
			Random r = new Random();
			ComboBox.ObjectCollection sourceRevenue = form.comboBoxRevenueSource.Items;
			for (DateTime i = start; i <= finish; i = i.AddDays(1))
			{
				if (r.Next(2) == 0)
				{
					DbManager.TableTransactionsRevenues.AddRow(new Transaction { DateTime = i, Money = r.Next(10000), });
				}
				if (r.Next(2) == 0)
				{
					DbManager.TableTransactionsExpences.AddRow(new Transaction { DateTime = i, Money = r.Next(10000) });
				}
			}
			FillData();
		}
		public static void Nowing(DateTimePicker picker)
		{
			picker.Value = DateTime.Now.Date;
		}
		private void DefaultingDateTimePickers()
		{
			ResetRevenueDateTimePickers();
			ResetExpencesDateTimePickers();
			ResetStatisticsDateTimePickers();
		}

		public void ResetStatisticsDateTimePickers()
		{
			dateSortRegulatorStatistics.ResetDateTimePickers();
		}

		public void ResetExpencesDateTimePickers()
		{
			Nowing(form.dateTimePickerExpencesDateAndTime);
			dateSortRegulatorExpences.ResetDateTimePickers();
		}

		public void ResetRevenueDateTimePickers()
		{
			Nowing(form.dateTimePickerRevenueDateAndTime);
			dateSortRegulatorRevenue.ResetDateTimePickers();
		}

		public override void FillData()
		{
			DbManager = DbManager;
			FillComboboxes();
			FillAllDataGridViews();
			FillBalanceFills();
			FillStatictic();
		}

		#region Balance

		public void FillBalanceFills()
		{
			DateTime start = form.dateTimePickerBalanceFrom.Value;
			DateTime finish = form.dateTimePickerBalanceTo.Value;
			decimal revenue = DbManager.TableTransactionsRevenues.SummByPeriod(start, finish);
			form.textBoxBalanceRevenue.Text = revenue.ToString();
			decimal expences = DbManager.TableTransactionsExpences.SummByPeriod(start, finish);
			form.textBoxBalanceExpences.Text = expences.ToString();
			decimal balance = revenue - expences;
			form.textBoxBalanceBalance.Text = balance.ToString();
			decimal revenueSumm;
			decimal expencesSumm;
			foreach (Series item in form.chartBalance.Series)
			{
				item.Points.Clear();
			}
			for (DateTime i = start; i.Date <= finish.Date; i = i.AddDays(1))
			{
				revenue = DbManager.TableTransactionsRevenues.SummByPeriod(i, i);
				expences = DbManager.TableTransactionsExpences.SummByPeriod(i, i);
				revenueSumm = DbManager.TableTransactionsRevenues.SummByPeriod(start, i);
				expencesSumm = DbManager.TableTransactionsExpences.SummByPeriod(start, i);
				balance = revenueSumm - expencesSumm;
				//balance = revenue - expences;
				form.chartBalance.Series[0].Points.AddXY(i, revenue);
				form.chartBalance.Series[1].Points.AddXY(i, -expences);
				form.chartBalance.Series[2].Points.AddXY(i, balance);
			}
		}
		#endregion
		#region ExpencesAndRevenues
		private void FillComboboxes()
		{
			form.comboBoxRevenueSource.DataSource = null;
			form.comboBoxRevenueSource.DataSource = DbManager.TableSourceTransactionsRevenues.Rows;
			form.comboBoxRevenueSource.DisplayMember = nameof(SourceTransactions.Name);
			form.comboBoxRevenueSource.SelectedIndex = -1;
			form.comboBoxExpencesSource.DataSource = null;
			form.comboBoxExpencesSource.DataSource = DbManager.TableSourceTransactionsExpences.Rows;
			form.comboBoxExpencesSource.DisplayMember = nameof(SourceTransactions.Name);
			form.comboBoxExpencesSource.SelectedIndex = -1;
		}

		#region DataGridViewFiller
		public void FillAllDataGridViews()
		{
			foreach (DataGridView item in GetDataGridViewsList())
			{
				FillDataGridView(item);
			}
		}

		public void FillDataGridView(DataGridView dataGridView)
		{
			FillDataGridView(dataGridView, GetTableTransactionByDataGridView(dataGridView).GetRowsAsList<Transaction>());
		}
		public void FillDataGridView(DataGridView dataGridView, DateTime dateTime)
		{
			FillDataGridView(dataGridView, GetTableTransactionByDataGridView(dataGridView).GetTransactionsByDate(dateTime));
		}
		public void FillDataGridView(DataGridView dataGridView, DateTime dateFrom, DateTime dateTo)
		{
			FillDataGridView(dataGridView, GetTableTransactionByDataGridView(dataGridView).GetTransactionsByDate(dateFrom, dateTo));
		}
		private TableTransactions GetTableTransactionByDataGridView(DataGridView dataGridView)
		{
			List<int> list = new List<int> { 1, 2, 3 };
			int number = GetDataGridViewsList().FindIndex(item => item == dataGridView);
			switch (number)
			{
				case 0:
				case 2:
					return DbManager.TableTransactionsRevenues;
				case 1:
				case 3:
					return DbManager.TableTransactionsExpences;
				default:
					return null;
			}
		}

		private List<DataGridView> GetDataGridViewsList()
		{
			return new List<DataGridView> {
				form.dataGridViewRevenue,
				form.dataGridViewExpences,
			};
		}


		private void FillDataGridView(DataGridView dataGridView, List<Transaction> dataSource)
		{
			dataGridView.DataSource = null;
			dataGridView.DataSource = dataSource;
			dataGridView.Columns[0].HeaderText = "Id";
			dataGridView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
			dataGridView.Columns[1].HeaderText = "Дата/Время";
			dataGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
			dataGridView.Columns[2].HeaderText = "Сумма";
			dataGridView.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
			dataGridView.Columns[3].HeaderText = "Источник";
		}
		#endregion
		internal void OpenFormSource(ER eR)
		{
			new ControllerFormSource(new FormSource(), eR).OpenForm();
			FillData();
		}
		public void DeleteSelectedER(ER where)
		{
			DataGridView dataGridView;
			if (where == ER.Revenues)
			{
				dataGridView = form.dataGridViewRevenue;
			}
			else
			{
				dataGridView = form.dataGridViewExpences;
			}
			TableTransactions tableTransactions = GetTableTransactionByDataGridView(dataGridView);

			if (dataGridView.SelectedRows.Count != 0)
			{
				foreach (DataGridViewRow row in dataGridView.SelectedRows)
				{
					tableTransactions.RemoveRow(int.Parse(row.Cells[0].Value.ToString()));
				}
				FillDataGridView(dataGridView, tableTransactions.GetRowsAsList<Transaction>());
			}

			FillData();
		}
		public void AddNewRevenueOrExpences(ER eR)
		{
			NumericUpDown numericUpDownSumm;
			ComboBox comboBoxSource;
			DateTimePicker dateTimePickerDateAndTime;
			DateTimePicker dateTimePickerOn;
			DateTimePicker dateTimePickerFrom;
			DateTimePicker dateTimePickerTo;
			if (eR == ER.Revenues)
			{
				numericUpDownSumm = form.numericUpDownRevenueSumm;
				comboBoxSource = form.comboBoxRevenueSource;
				dateTimePickerDateAndTime = form.dateTimePickerRevenueDateAndTime;
				dateTimePickerOn = form.dateTimePickerRevenueOn;
				dateTimePickerFrom = form.dateTimePickerRevenueFrom;
				dateTimePickerTo = form.dateTimePickerRevenueTo;
			}
			else
			{
				numericUpDownSumm = form.numericUpDownExpencesSumm;
				comboBoxSource = form.comboBoxExpencesSource;
				dateTimePickerDateAndTime = form.dateTimePickerExpencesDateAndTime;
				dateTimePickerOn = form.dateTimePickerExpencesOn;
				dateTimePickerFrom = form.dateTimePickerExpencesFrom;
				dateTimePickerTo = form.dateTimePickerExpencesTo;
			}
			if (comboBoxSource.SelectedIndex == -1)
			{
				MessageBox.Show("Выберите источник");
			}
			Transaction transaction = new Transaction()
			{
				DateTime = dateTimePickerDateAndTime.Value,
				Money = numericUpDownSumm.Value,
				SourceTransactions = (SourceTransactions)comboBoxSource.SelectedItem
			};
			if (eR == ER.Revenues)
			{
				DbManager.TableTransactionsRevenues.AddRow(transaction);
			}
			else
			{
				DbManager.TableTransactionsExpences.AddRow(transaction);
			}
			numericUpDownSumm.Value = 0;
			comboBoxSource.SelectedIndex = -1;
			dateTimePickerOn.Value = dateTimePickerDateAndTime.Value;
			dateTimePickerFrom.Value = new DateTime(Math.Min(dateTimePickerFrom.Value.Ticks, dateTimePickerDateAndTime.Value.Ticks));
			dateTimePickerTo.Value = new DateTime(Math.Max(dateTimePickerTo.Value.Ticks, dateTimePickerDateAndTime.Value.Ticks));
			FillData();
		}
		#endregion
		#region Statistic
		private class TextBoxStatisticsFiller
		{
			public List<(string, string, string, string)> text;
			public TextBox textBox;
			public TextBoxStatisticsFiller(TextBox textBox)
			{
				text = new List<(string, string, string, string)>();
				this.textBox = textBox;
				text.Add(("", "Доход", "Расход", "Баланс"));
			}


			public void FillTextBox()
			{
				textBox.Clear();
				foreach ((string, string, string, string) row in text)
				{
					textBox.Text +=
						SetLength(row.Item1, 20) + "\t" +
						SetLength(row.Item2, 20) + "\t" +
						SetLength(row.Item3, 20) + "\t" +
						SetLength(row.Item4, 20) + "\t" +
						Environment.NewLine;
				}
				string SetLength(string s, int length)
				{
					string v = s.PadRight(length);
					int startIndex = length - 1;
					return v.Remove(startIndex);
				}
			}
			internal void Add(string key, string v1, string v2, string v3)
			{
				text.Add((key, v1, v2, v3));
			}
		}
		public void FillStatictic()
		{
			List<Transaction> revenues = GetDatedList(DbManager.TableTransactionsRevenues);
			List<Transaction> expences = GetDatedList(DbManager.TableTransactionsExpences);
			List<Transaction> balance = new List<Transaction>(1);
			if (revenues.Count != 0 || expences.Count != 0)
			{
				DateTime start = DateTime.MaxValue;
				DateTime finish = DateTime.MinValue;
				foreach (Transaction item in revenues)
				{
					if (item.DateTime < start)
					{
						start = item.DateTime;
					}
					if (item.DateTime > finish)
					{
						finish = item.DateTime;
					}
				}
				foreach (Transaction item in expences)
				{
					if (item.DateTime < start)
					{
						start = item.DateTime;
					}
					if (item.DateTime > finish)
					{
						finish = item.DateTime;
					}
				}
				for (DateTime date = start; date.Date <= finish.Date; date = date.AddDays(1))
				{
					balance.Add(new Transaction()
					{
						DateTime = date,
						Money =
						revenues.Sum(item => item.DateTime <= date ? item.Money : 0) -
						expences.Sum(item => item.DateTime <= date ? item.Money : 0)
					});
				}
			}


			textBoxStatisticsFiller = new TextBoxStatisticsFiller(form.textBoxStatistics);
			AddPositionToStatistic("Максимальный", list =>
			{
				if (list.Count == 0)
				{
					return "0";
				}
				return list.Max(item => item.Money).ToString();
			});
			AddPositionToStatistic("Минимальный", list =>
			{
				if (list.Count == 0)
				{
					return "0";
				}
				return list.Min(item => item.Money).ToString();
			});
			AddPositionToStatistic("Средний", list =>
			{
				if (list.Count == 0)
				{
					return "0";
				}
				return (list.Sum(item => item.Money) / list.Count).ToString();
			});
			AddPositionToStatistic("Всего", list =>
			{
				if (list.Count == 0)
				{
					return "0";
				}
				if (list == balance)
				{
					return list.Last().Money.ToString();
				}
				return list.Sum(item => item.Money).ToString();
			});
			textBoxStatisticsFiller.FillTextBox();

			void AddPositionToStatistic(string key, Func<List<Transaction>, string> func)
			{
				textBoxStatisticsFiller.Add(key, func(revenues), func(expences), func(balance));
			}
			List<Transaction> GetDatedList(TableTransactions tableTransactions)
			{
				List<Transaction> result = new List<Transaction>();
				if (form.radioButtonStatisticsAllTime.Checked)
				{
					result = tableTransactions.GetRowsAsList<Transaction>();
				}
				else if (form.radioButtonStatisticsOneDay.Checked)
				{
					result = tableTransactions.GetTransactionsByDate(form.dateTimePickerStatisticsOn.Value);
				}
				else if (form.radioButtonStatisticsPeriod.Checked)
				{
					result = tableTransactions.GetTransactionsByDate(form.dateTimePickerStatisticsFrom.Value, form.dateTimePickerStatisticsTo.Value);
				}
				return result;
			}
		}


		#endregion
	}
}

