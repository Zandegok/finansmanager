﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinansManager
{
	abstract class ControllerForm
	{
		protected Form form;
		private DbManager dbManager;
		public ControllerForm(Form form)
		{
			this.form = form ?? throw new ArgumentNullException(nameof(form));
			DbManager = DbManager.Inctance;
		}

		protected DbManager DbManager { get => DbManager.Inctance; set => dbManager = value; }

		public abstract void FillData();
	}
}
