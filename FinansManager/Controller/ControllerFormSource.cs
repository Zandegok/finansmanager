﻿using FinansManager.View;
using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinansManager
{
	class ControllerFormSource : ControllerForm
	{
		new public FormSource form;
		private FormSourceMessageBox messageBox;
		ER WhoIAm;
		public ControllerFormSource(FormSource form, ER WhoIAm) : base(form)
		{
			this.form = form;
			form.controller = this;
			this.WhoIAm = WhoIAm;
			messageBox = new FormSourceMessageBox();
		}

		public override void FillData()
		{
			form.listBox1.DataSource = null;
			if (WhoIAm == ER.Revenues)
			{
				form.listBox1.DataSource = DbManager.TableSourceTransactionsRevenues.Rows;
			}
			else
			{
				form.listBox1.DataSource = DbManager.TableSourceTransactionsExpences.Rows;

			}
			form.listBox1.DisplayMember = nameof(SourceTransactions.Name);
		}

		internal void EditSelectedSource()
		{
			if (form.listBox1.SelectedItems.Count!=1)
			{
				MessageBox.Show("Выберите 1 источник");
				return;
			}
			if (form.textBoxSourceName.Text.Length == 0)
			{
				MessageBox.Show("Введите новое имя");
				return;
			}
			string text = form.textBoxSourceName.Text;
			TableSourceTransactions tableSourceTransactions =WhoIAm==ER.Revenues?DbManager.TableSourceTransactionsRevenues:DbManager.TableSourceTransactionsExpences;
			if (tableSourceTransactions.ExistByName(text))
			{
				MessageBox.Show("Источник с таким именем уже существует имя");
				return;
			}

			(form.listBox1.SelectedItem as SourceTransactions).Name = text;
			FillData();
		}

		internal void AntiSelect()
		{
			form.listBox1.SelectedIndex = -1;
		}

		internal void DeleteSelectedSource()
		{
			if (form.listBox1.SelectedIndex == -1)
			{
				return;
			}
			TableTransactions tableTransactions = ER.Revenues == WhoIAm ? DbManager.TableTransactionsRevenues : DbManager.TableTransactionsExpences;
			TableSourceTransactions tableSourceTransactions = ER.Revenues == WhoIAm ? DbManager.TableSourceTransactionsRevenues : DbManager.TableSourceTransactionsExpences;
			DialogResult lastDialogResult = DialogResult.None;
			foreach (SourceTransactions item in form.listBox1.SelectedItems)
			{
				if (tableTransactions.RowsContain<Transaction>(i => i.SourceTransactions == item))
				{
					if (lastDialogResult==DialogResult.None)
					{
						lastDialogResult = messageBox.Show($"Выбранный источник ({item.Name}) используется.{Environment.NewLine} Вы хотите удалить его все транзакции с ним?"
						);
					}
					switch (lastDialogResult)
					{
						case DialogResult.OK:
							tableTransactions.Rows.RemoveAll(i=>(i as Transaction).SourceTransactions==item);
							break;
						case DialogResult.Cancel:
							continue;
					}
					if (!messageBox.checkBox1.Checked)
					{
						lastDialogResult = DialogResult.None;
					}
				}
				tableSourceTransactions.RemoveRow(item.ID);
			}
			messageBox.checkBox1.Checked = false;
			FillData();
		}

		public void AddSource()
		{
			string source = form.textBoxSourceName.Text;
			if (source.Length == 0)
			{
				MessageBox.Show("Ошибка заполните поле");
				return;
			}
			SourceTransactions sourceTransactions = new SourceTransactions() { Name = source };
			TableSourceTransactions tableSourceTransactions = WhoIAm == ER.Revenues ? DbManager.TableSourceTransactionsRevenues : DbManager.TableSourceTransactionsExpences;
			if (tableSourceTransactions.ExistByName(source))
			{
				MessageBox.Show("Источник уже существует");
				return;
			}
			tableSourceTransactions.AddRow(sourceTransactions);
			FillData();
			ClearFields();
		}
		public DialogResult OpenForm()
		{
			FillData();
			form.ShowDialog();
			return form.DialogResult;
		}

		private void ClearFields()
		{
			form.textBoxSourceName.Clear();
		}
	}
}
