﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinansManager
{
	[Serializable]
	class Transaction : IDable
	{
		public int ID { get; set; }

		public DateTime DateTime { get; set; }
		public decimal Money { get; set; }
		public SourceTransactions SourceTransactions { get; set; }
	}
}
