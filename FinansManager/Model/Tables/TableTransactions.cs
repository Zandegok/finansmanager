﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinansManager

{
	[Serializable]
	class TableTransactions : Table
	{
		public TableTransactions():base()
		{
			
		}
		public List<Transaction> GetTransactionsByDate(DateTime time)
		{
			return GetTransactionsByDate(time, time);
		}
		public List<Transaction> GetTransactionsByDate(DateTime timeFrom,DateTime timeTo)
		{
			List<Transaction> tableTransactions = GetRowsAsList<Transaction>();
			if (tableTransactions == null) return null;
			return tableTransactions.FindAll(item => item.DateTime>=timeFrom.Date&&item.DateTime.Date<=timeTo.Date);
		}

		public decimal SummByPeriod(DateTime start,DateTime finish)
		{
			return GetTransactionsByDate(start, finish).Sum(item => item.Money);
		}
	}
}
