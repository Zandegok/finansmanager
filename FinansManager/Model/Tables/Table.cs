﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace FinansManager
{
	[Serializable]
	abstract class Table : IDable
	{
		public int ID { get; set; }
		public List<IDable> Rows { get; set; }
		public List<T> GetRowsAsList<T>()where T:class
		{
			if (Rows == null) return null;
			return Rows.ConvertAll(item => item as T);
		}
		public Table()
		{
			ID = 1;
			Rows = new List<IDable>();
		}
		public bool RowsContain<T>(Func<T, bool> func)where T:class
		{
			foreach (var item in GetRowsAsList<T>())
			{
				if (func(item))
				{
					return true;
				}
			}
				return false;
		}
		public void AddRow(IDable item)
		{
			item.ID = ID++;
			Rows.Add(item);
		}
		public void RemoveRow(int id)
		{
			Rows.Remove(Rows.Find(item => item.ID == id));
		}
	}
}