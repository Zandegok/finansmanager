﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinansManager
{
	[Serializable]
	class TableSourceTransactions : Table
	{
		public TableSourceTransactions() : base()
		{
		}
		public bool ExistByName(string name)
		{
			try
			{
				return GetRowsAsList<SourceTransactions>().Exists(item => item.Name == name);
			}
			catch (NullReferenceException) { return false; }
		}
	}
}
