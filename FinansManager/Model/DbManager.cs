﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
namespace FinansManager
{
   public enum ER {Excpences,Revenues}
	[Serializable]
	internal class DbManager
	{
		private static DbManager inctance = null;
		private static string FILE_NAME = "db.txt";

		private DbManager()
		{
			TableTransactionsRevenues = new TableTransactions();
			TableTransactionsExpences = new TableTransactions();
			TableSourceTransactionsRevenues = new TableSourceTransactions();
			TableSourceTransactionsExpences = new TableSourceTransactions();
		}
		~DbManager()
		{
			Save();
		}

		private void Save()
		{
			try
			{
				FileStream fs = new FileStream(FILE_NAME, FileMode.OpenOrCreate, FileAccess.Write);
				try
				{
					BinaryFormatter formatter = new BinaryFormatter();
					formatter.Serialize(fs, inctance);
				}
				finally
				{
					fs.Close();
				}
			}
			catch
			{
				return;
			}
		}
		private static DbManager Load()
		{

			try
			{
				FileStream fs = new FileStream(FILE_NAME, FileMode.Open, FileAccess.Read);


				try
				{
					BinaryFormatter formatter = new BinaryFormatter();
					DbManager dbManager = (DbManager)formatter.Deserialize(fs);
					if (dbManager==null)
					{
						return new DbManager();
					}
					return dbManager;
				}
				finally
				{
					fs.Close();
				}
			}
			catch
			{
			}
			return new DbManager();

		}

		public TableTransactions TableTransactionsRevenues { get; set; }
		public TableTransactions TableTransactionsExpences { get; set; }
		public TableSourceTransactions TableSourceTransactionsRevenues { get; set; }
		public TableSourceTransactions TableSourceTransactionsExpences { get; set; }
		internal static DbManager Inctance
		{
			get
			{
				try
				{
					if (inctance == null)
					{
						if (File.Exists(FILE_NAME))
						{
							inctance= Load();
						}
						else
						{
							inctance =new DbManager();
						}
					}
				}
				catch { inctance= new DbManager();}
					return inctance;
			}
		}
	}
}
