﻿using System;

namespace FinansManager
{
	interface IDable
	{
		int ID { get; set; }
	}
}