﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinansManager
{
	public partial class FormSource : Form
	{
	internal ControllerFormSource controller;

		internal FormSource()
		{
			InitializeComponent();
		}

		private void buttonAdd_Click(object sender, EventArgs e)
		{
			controller.AddSource();
		}

		private void buttonEdit_Click(object sender, EventArgs e)
		{
			controller.EditSelectedSource();
		}

		private void buttonDelete_Click(object sender, EventArgs e)
		{
			controller.DeleteSelectedSource();
		}

		private void textBoxSourceName_TextChanged(object sender, EventArgs e)
		{
			controller.AntiSelect();
		}
	}
}
