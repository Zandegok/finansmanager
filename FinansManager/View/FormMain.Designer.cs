﻿using System;

namespace FinansManager
{
	partial class FormMain
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.Balance = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.chartBalance = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.dateTimePickerBalanceTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dateTimePickerBalanceFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.textBoxBalanceExpences = new System.Windows.Forms.TextBox();
			this.textBoxBalanceRevenue = new System.Windows.Forms.TextBox();
			this.textBoxBalanceBalance = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.Revenue = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.label26 = new System.Windows.Forms.Label();
			this.radioButtonRevenueAllTime = new System.Windows.Forms.RadioButton();
			this.dateTimePickerRevenueDateAndTime = new System.Windows.Forms.DateTimePicker();
			this.radioButtonRevenueOneDay = new System.Windows.Forms.RadioButton();
			this.label27 = new System.Windows.Forms.Label();
			this.radioButtonRevenuePeriod = new System.Windows.Forms.RadioButton();
			this.label28 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.comboBoxRevenueSource = new System.Windows.Forms.ComboBox();
			this.label9 = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.dateTimePickerRevenueOn = new System.Windows.Forms.DateTimePicker();
			this.button3 = new System.Windows.Forms.Button();
			this.label20 = new System.Windows.Forms.Label();
			this.numericUpDownRevenueSumm = new System.Windows.Forms.NumericUpDown();
			this.dateTimePickerRevenueFrom = new System.Windows.Forms.DateTimePicker();
			this.button4 = new System.Windows.Forms.Button();
			this.label21 = new System.Windows.Forms.Label();
			this.button5 = new System.Windows.Forms.Button();
			this.dateTimePickerRevenueTo = new System.Windows.Forms.DateTimePicker();
			this.dataGridViewRevenue = new System.Windows.Forms.DataGridView();
			this.Expenses = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label10 = new System.Windows.Forms.Label();
			this.radioButtonExpencesAllTime = new System.Windows.Forms.RadioButton();
			this.dateTimePickerExpencesDateAndTime = new System.Windows.Forms.DateTimePicker();
			this.radioButtonExpencesOneDay = new System.Windows.Forms.RadioButton();
			this.label11 = new System.Windows.Forms.Label();
			this.radioButtonExpencesPeriod = new System.Windows.Forms.RadioButton();
			this.label12 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.comboBoxExpencesSource = new System.Windows.Forms.ComboBox();
			this.label13 = new System.Windows.Forms.Label();
			this.buttonAddExpencesTransaction = new System.Windows.Forms.Button();
			this.dateTimePickerExpencesOn = new System.Windows.Forms.DateTimePicker();
			this.buttonExpencesSourceEdit = new System.Windows.Forms.Button();
			this.label24 = new System.Windows.Forms.Label();
			this.numericUpDownExpencesSumm = new System.Windows.Forms.NumericUpDown();
			this.dateTimePickerExpencesFrom = new System.Windows.Forms.DateTimePicker();
			this.buttonExpencesRemoveSelectedRows = new System.Windows.Forms.Button();
			this.label25 = new System.Windows.Forms.Label();
			this.buttonExpencesDateCancel = new System.Windows.Forms.Button();
			this.dateTimePickerExpencesTo = new System.Windows.Forms.DateTimePicker();
			this.dataGridViewExpences = new System.Windows.Forms.DataGridView();
			this.Statistic = new System.Windows.Forms.TabPage();
			this.radioButtonStatisticsAllTime = new System.Windows.Forms.RadioButton();
			this.radioButtonStatisticsOneDay = new System.Windows.Forms.RadioButton();
			this.radioButtonStatisticsPeriod = new System.Windows.Forms.RadioButton();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.dateTimePickerStatisticsOn = new System.Windows.Forms.DateTimePicker();
			this.label8 = new System.Windows.Forms.Label();
			this.dateTimePickerStatisticsTo = new System.Windows.Forms.DateTimePicker();
			this.label16 = new System.Windows.Forms.Label();
			this.dateTimePickerStatisticsFrom = new System.Windows.Forms.DateTimePicker();
			this.textBoxStatistics = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.tabControl1.SuspendLayout();
			this.Balance.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chartBalance)).BeginInit();
			this.tableLayoutPanel2.SuspendLayout();
			this.tableLayoutPanel3.SuspendLayout();
			this.Revenue.SuspendLayout();
			this.tableLayoutPanel10.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownRevenueSumm)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewRevenue)).BeginInit();
			this.Expenses.SuspendLayout();
			this.tableLayoutPanel9.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownExpencesSumm)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewExpences)).BeginInit();
			this.Statistic.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.Balance);
			this.tabControl1.Controls.Add(this.Revenue);
			this.tabControl1.Controls.Add(this.Expenses);
			this.tabControl1.Controls.Add(this.Statistic);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(1407, 606);
			this.tabControl1.TabIndex = 0;
			// 
			// Balance
			// 
			this.Balance.Controls.Add(this.tableLayoutPanel1);
			this.Balance.Controls.Add(this.button1);
			this.Balance.Location = new System.Drawing.Point(4, 25);
			this.Balance.Name = "Balance";
			this.Balance.Padding = new System.Windows.Forms.Padding(3);
			this.Balance.Size = new System.Drawing.Size(1399, 577);
			this.Balance.TabIndex = 0;
			this.Balance.Text = "Баланс";
			this.Balance.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.chartBalance, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(1393, 571);
			this.tableLayoutPanel1.TabIndex = 12;
			// 
			// chartBalance
			// 
			chartArea1.Name = "ChartArea1";
			this.chartBalance.ChartAreas.Add(chartArea1);
			this.chartBalance.Dock = System.Windows.Forms.DockStyle.Fill;
			legend1.Name = "Legend1";
			this.chartBalance.Legends.Add(legend1);
			this.chartBalance.Location = new System.Drawing.Point(3, 88);
			this.chartBalance.Name = "chartBalance";
			series1.ChartArea = "ChartArea1";
			series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
			series1.LabelAngle = 90;
			series1.LabelBackColor = System.Drawing.Color.Transparent;
			series1.LabelBorderColor = System.Drawing.Color.Transparent;
			series1.LabelBorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDot;
			series1.Legend = "Legend1";
			series1.Name = "Доходы";
			series2.ChartArea = "ChartArea1";
			series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
			series2.Legend = "Legend1";
			series2.Name = "Расходы";
			series3.BorderWidth = 2;
			series3.ChartArea = "ChartArea1";
			series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series3.LabelBackColor = System.Drawing.Color.RosyBrown;
			series3.Legend = "Legend1";
			series3.MarkerBorderWidth = 5;
			series3.Name = "Баланс";
			series3.YValuesPerPoint = 4;
			this.chartBalance.Series.Add(series1);
			this.chartBalance.Series.Add(series2);
			this.chartBalance.Series.Add(series3);
			this.chartBalance.Size = new System.Drawing.Size(1387, 480);
			this.chartBalance.TabIndex = 4;
			this.chartBalance.Text = "chart1";
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.AutoSize = true;
			this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanel2.ColumnCount = 4;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.Controls.Add(this.dateTimePickerBalanceTo, 3, 0);
			this.tableLayoutPanel2.Controls.Add(this.label2, 2, 0);
			this.tableLayoutPanel2.Controls.Add(this.dateTimePickerBalanceFrom, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 1;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.Size = new System.Drawing.Size(285, 28);
			this.tableLayoutPanel2.TabIndex = 5;
			// 
			// dateTimePickerBalanceTo
			// 
			this.dateTimePickerBalanceTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerBalanceTo.Location = new System.Drawing.Point(173, 3);
			this.dateTimePickerBalanceTo.Name = "dateTimePickerBalanceTo";
			this.dateTimePickerBalanceTo.Size = new System.Drawing.Size(109, 22);
			this.dateTimePickerBalanceTo.TabIndex = 3;
			this.dateTimePickerBalanceTo.ValueChanged += new System.EventHandler(this.dateTimePickerBalance_ValueChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(141, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(26, 17);
			this.label2.TabIndex = 2;
			this.label2.Text = "По";
			// 
			// dateTimePickerBalanceFrom
			// 
			this.dateTimePickerBalanceFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerBalanceFrom.Location = new System.Drawing.Point(26, 3);
			this.dateTimePickerBalanceFrom.Name = "dateTimePickerBalanceFrom";
			this.dateTimePickerBalanceFrom.Size = new System.Drawing.Size(109, 22);
			this.dateTimePickerBalanceFrom.TabIndex = 1;
			this.dateTimePickerBalanceFrom.ValueChanged += new System.EventHandler(this.dateTimePickerBalance_ValueChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(17, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "С";
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.AutoSize = true;
			this.tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanel3.ColumnCount = 3;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel3.Controls.Add(this.label3, 0, 0);
			this.tableLayoutPanel3.Controls.Add(this.label4, 1, 0);
			this.tableLayoutPanel3.Controls.Add(this.label5, 2, 0);
			this.tableLayoutPanel3.Controls.Add(this.textBoxBalanceExpences, 1, 1);
			this.tableLayoutPanel3.Controls.Add(this.textBoxBalanceRevenue, 0, 1);
			this.tableLayoutPanel3.Controls.Add(this.textBoxBalanceBalance, 2, 1);
			this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 37);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 2;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel3.Size = new System.Drawing.Size(300, 45);
			this.tableLayoutPanel3.TabIndex = 6;
			// 
			// label3
			// 
			this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(25, 0);
			this.label3.Name = "label3";
			this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.label3.Size = new System.Drawing.Size(49, 17);
			this.label3.TabIndex = 5;
			this.label3.Text = "Доход";
			// 
			// label4
			// 
			this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(123, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(54, 17);
			this.label4.TabIndex = 7;
			this.label4.Text = "Расход";
			// 
			// label5
			// 
			this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(222, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(56, 17);
			this.label5.TabIndex = 9;
			this.label5.Text = "Баланс";
			// 
			// textBoxBalanceExpences
			// 
			this.textBoxBalanceExpences.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.textBoxBalanceExpences.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxBalanceExpences.Location = new System.Drawing.Point(103, 20);
			this.textBoxBalanceExpences.Name = "textBoxBalanceExpences";
			this.textBoxBalanceExpences.ReadOnly = true;
			this.textBoxBalanceExpences.Size = new System.Drawing.Size(94, 22);
			this.textBoxBalanceExpences.TabIndex = 8;
			// 
			// textBoxBalanceRevenue
			// 
			this.textBoxBalanceRevenue.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.textBoxBalanceRevenue.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxBalanceRevenue.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.textBoxBalanceRevenue.Location = new System.Drawing.Point(3, 20);
			this.textBoxBalanceRevenue.Name = "textBoxBalanceRevenue";
			this.textBoxBalanceRevenue.ReadOnly = true;
			this.textBoxBalanceRevenue.Size = new System.Drawing.Size(94, 22);
			this.textBoxBalanceRevenue.TabIndex = 6;
			// 
			// textBoxBalanceBalance
			// 
			this.textBoxBalanceBalance.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.textBoxBalanceBalance.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxBalanceBalance.Location = new System.Drawing.Point(203, 20);
			this.textBoxBalanceBalance.Name = "textBoxBalanceBalance";
			this.textBoxBalanceBalance.ReadOnly = true;
			this.textBoxBalanceBalance.Size = new System.Drawing.Size(94, 22);
			this.textBoxBalanceBalance.TabIndex = 10;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(347, 214);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 11;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// Revenue
			// 
			this.Revenue.Controls.Add(this.tableLayoutPanel10);
			this.Revenue.Location = new System.Drawing.Point(4, 25);
			this.Revenue.Name = "Revenue";
			this.Revenue.Padding = new System.Windows.Forms.Padding(3);
			this.Revenue.Size = new System.Drawing.Size(1399, 577);
			this.Revenue.TabIndex = 1;
			this.Revenue.Text = "Доходы";
			this.Revenue.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel10
			// 
			this.tableLayoutPanel10.ColumnCount = 1;
			this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel10.Controls.Add(this.groupBox2, 0, 0);
			this.tableLayoutPanel10.Controls.Add(this.dataGridViewRevenue, 0, 1);
			this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel10.Name = "tableLayoutPanel10";
			this.tableLayoutPanel10.RowCount = 2;
			this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel10.Size = new System.Drawing.Size(1393, 571);
			this.tableLayoutPanel10.TabIndex = 30;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label26);
			this.groupBox2.Controls.Add(this.radioButtonRevenueAllTime);
			this.groupBox2.Controls.Add(this.dateTimePickerRevenueDateAndTime);
			this.groupBox2.Controls.Add(this.radioButtonRevenueOneDay);
			this.groupBox2.Controls.Add(this.label27);
			this.groupBox2.Controls.Add(this.radioButtonRevenuePeriod);
			this.groupBox2.Controls.Add(this.label28);
			this.groupBox2.Controls.Add(this.label22);
			this.groupBox2.Controls.Add(this.comboBoxRevenueSource);
			this.groupBox2.Controls.Add(this.label9);
			this.groupBox2.Controls.Add(this.button2);
			this.groupBox2.Controls.Add(this.dateTimePickerRevenueOn);
			this.groupBox2.Controls.Add(this.button3);
			this.groupBox2.Controls.Add(this.label20);
			this.groupBox2.Controls.Add(this.numericUpDownRevenueSumm);
			this.groupBox2.Controls.Add(this.dateTimePickerRevenueFrom);
			this.groupBox2.Controls.Add(this.button4);
			this.groupBox2.Controls.Add(this.label21);
			this.groupBox2.Controls.Add(this.button5);
			this.groupBox2.Controls.Add(this.dateTimePickerRevenueTo);
			this.groupBox2.Location = new System.Drawing.Point(3, 3);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(696, 173);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Location = new System.Drawing.Point(6, 18);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(98, 17);
			this.label26.TabIndex = 0;
			this.label26.Text = "Дата и время";
			// 
			// radioButtonRevenueAllTime
			// 
			this.radioButtonRevenueAllTime.AutoSize = true;
			this.radioButtonRevenueAllTime.Checked = true;
			this.radioButtonRevenueAllTime.Location = new System.Drawing.Point(6, 84);
			this.radioButtonRevenueAllTime.Name = "radioButtonRevenueAllTime";
			this.radioButtonRevenueAllTime.Size = new System.Drawing.Size(17, 16);
			this.radioButtonRevenueAllTime.TabIndex = 21;
			this.radioButtonRevenueAllTime.TabStop = true;
			this.radioButtonRevenueAllTime.UseVisualStyleBackColor = true;
			this.radioButtonRevenueAllTime.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			this.radioButtonRevenueAllTime.Click += new System.EventHandler(this.radioButton1_CheckedChanged);
			// 
			// dateTimePickerRevenueDateAndTime
			// 
			this.dateTimePickerRevenueDateAndTime.CustomFormat = "d.MM.yyyy H:mm";
			this.dateTimePickerRevenueDateAndTime.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerRevenueDateAndTime.Location = new System.Drawing.Point(110, 17);
			this.dateTimePickerRevenueDateAndTime.Name = "dateTimePickerRevenueDateAndTime";
			this.dateTimePickerRevenueDateAndTime.Size = new System.Drawing.Size(107, 22);
			this.dateTimePickerRevenueDateAndTime.TabIndex = 1;
			// 
			// radioButtonRevenueOneDay
			// 
			this.radioButtonRevenueOneDay.AutoSize = true;
			this.radioButtonRevenueOneDay.Location = new System.Drawing.Point(6, 109);
			this.radioButtonRevenueOneDay.Name = "radioButtonRevenueOneDay";
			this.radioButtonRevenueOneDay.Size = new System.Drawing.Size(17, 16);
			this.radioButtonRevenueOneDay.TabIndex = 22;
			this.radioButtonRevenueOneDay.UseVisualStyleBackColor = true;
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Location = new System.Drawing.Point(260, 18);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(50, 17);
			this.label27.TabIndex = 2;
			this.label27.Text = "Сумма";
			// 
			// radioButtonRevenuePeriod
			// 
			this.radioButtonRevenuePeriod.AutoSize = true;
			this.radioButtonRevenuePeriod.Location = new System.Drawing.Point(6, 135);
			this.radioButtonRevenuePeriod.Name = "radioButtonRevenuePeriod";
			this.radioButtonRevenuePeriod.Size = new System.Drawing.Size(17, 16);
			this.radioButtonRevenuePeriod.TabIndex = 23;
			this.radioButtonRevenuePeriod.TabStop = true;
			this.radioButtonRevenuePeriod.UseVisualStyleBackColor = true;
			// 
			// label28
			// 
			this.label28.AutoSize = true;
			this.label28.Location = new System.Drawing.Point(423, 18);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(71, 17);
			this.label28.TabIndex = 4;
			this.label28.Text = "Источник";
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(24, 84);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(91, 17);
			this.label22.TabIndex = 24;
			this.label22.Text = "Все расходы";
			// 
			// comboBoxRevenueSource
			// 
			this.comboBoxRevenueSource.FormattingEnabled = true;
			this.comboBoxRevenueSource.Location = new System.Drawing.Point(501, 17);
			this.comboBoxRevenueSource.Name = "comboBoxRevenueSource";
			this.comboBoxRevenueSource.Size = new System.Drawing.Size(121, 24);
			this.comboBoxRevenueSource.TabIndex = 5;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Enabled = false;
			this.label9.Location = new System.Drawing.Point(24, 108);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(83, 17);
			this.label9.TabIndex = 7;
			this.label9.Text = "Расходы за";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(462, 48);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(160, 23);
			this.button2.TabIndex = 6;
			this.button2.Text = "Добавить строку";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.buttonAddRevenueTransaction_Click);
			// 
			// dateTimePickerRevenueOn
			// 
			this.dateTimePickerRevenueOn.CustomFormat = "d.MM.yyyy H:mm";
			this.dateTimePickerRevenueOn.Enabled = false;
			this.dateTimePickerRevenueOn.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerRevenueOn.Location = new System.Drawing.Point(113, 108);
			this.dateTimePickerRevenueOn.Name = "dateTimePickerRevenueOn";
			this.dateTimePickerRevenueOn.Size = new System.Drawing.Size(107, 22);
			this.dateTimePickerRevenueOn.TabIndex = 8;
			this.dateTimePickerRevenueOn.ValueChanged += new System.EventHandler(this.dateTimePickerRevenueOn_ValueChanged);
			// 
			// button3
			// 
			this.button3.BackgroundImage = global::FinansManager.Properties.Resources.EditButtonIco;
			this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.button3.Location = new System.Drawing.Point(628, 16);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(61, 54);
			this.button3.TabIndex = 10;
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.buttonRevenueSourceEdit_Click);
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Enabled = false;
			this.label20.Location = new System.Drawing.Point(24, 135);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(75, 17);
			this.label20.TabIndex = 25;
			this.label20.Text = "Расходы с";
			// 
			// numericUpDownRevenueSumm
			// 
			this.numericUpDownRevenueSumm.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
			this.numericUpDownRevenueSumm.Location = new System.Drawing.Point(316, 17);
			this.numericUpDownRevenueSumm.Name = "numericUpDownRevenueSumm";
			this.numericUpDownRevenueSumm.Size = new System.Drawing.Size(100, 22);
			this.numericUpDownRevenueSumm.TabIndex = 11;
			// 
			// dateTimePickerRevenueFrom
			// 
			this.dateTimePickerRevenueFrom.CustomFormat = "d.MM.yyyy H:mm";
			this.dateTimePickerRevenueFrom.Enabled = false;
			this.dateTimePickerRevenueFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerRevenueFrom.Location = new System.Drawing.Point(113, 135);
			this.dateTimePickerRevenueFrom.Name = "dateTimePickerRevenueFrom";
			this.dateTimePickerRevenueFrom.Size = new System.Drawing.Size(107, 22);
			this.dateTimePickerRevenueFrom.TabIndex = 26;
			this.dateTimePickerRevenueFrom.ValueChanged += new System.EventHandler(this.dateTimePickerRevenue_ValueChanged);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(238, 48);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(169, 23);
			this.button4.TabIndex = 12;
			this.button4.Text = "Удалить выделение";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.buttonRevenueRemoveSelectedRows_Click);
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Enabled = false;
			this.label21.Location = new System.Drawing.Point(226, 134);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(24, 17);
			this.label21.TabIndex = 27;
			this.label21.Text = "по";
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(9, 47);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(170, 23);
			this.button5.TabIndex = 13;
			this.button5.Text = "Сброс даты";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.buttonRevenueDateCancel_Click);
			// 
			// dateTimePickerRevenueTo
			// 
			this.dateTimePickerRevenueTo.CustomFormat = "d.MM.yyyy H:mm";
			this.dateTimePickerRevenueTo.Enabled = false;
			this.dateTimePickerRevenueTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerRevenueTo.Location = new System.Drawing.Point(256, 135);
			this.dateTimePickerRevenueTo.Name = "dateTimePickerRevenueTo";
			this.dateTimePickerRevenueTo.Size = new System.Drawing.Size(109, 22);
			this.dateTimePickerRevenueTo.TabIndex = 28;
			this.dateTimePickerRevenueTo.ValueChanged += new System.EventHandler(this.dateTimePickerRevenue_ValueChanged);
			// 
			// dataGridViewRevenue
			// 
			this.dataGridViewRevenue.AllowUserToAddRows = false;
			this.dataGridViewRevenue.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dataGridViewRevenue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewRevenue.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewRevenue.Location = new System.Drawing.Point(3, 182);
			this.dataGridViewRevenue.Name = "dataGridViewRevenue";
			this.dataGridViewRevenue.RowHeadersWidth = 51;
			this.dataGridViewRevenue.RowTemplate.Height = 24;
			this.dataGridViewRevenue.Size = new System.Drawing.Size(1431, 386);
			this.dataGridViewRevenue.TabIndex = 9;
			// 
			// Expenses
			// 
			this.Expenses.Controls.Add(this.tableLayoutPanel9);
			this.Expenses.Location = new System.Drawing.Point(4, 25);
			this.Expenses.Name = "Expenses";
			this.Expenses.Size = new System.Drawing.Size(1399, 577);
			this.Expenses.TabIndex = 2;
			this.Expenses.Text = "Расходы";
			this.Expenses.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel9
			// 
			this.tableLayoutPanel9.ColumnCount = 1;
			this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel9.Controls.Add(this.groupBox1, 0, 0);
			this.tableLayoutPanel9.Controls.Add(this.dataGridViewExpences, 0, 1);
			this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel9.Name = "tableLayoutPanel9";
			this.tableLayoutPanel9.RowCount = 2;
			this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel9.Size = new System.Drawing.Size(1399, 577);
			this.tableLayoutPanel9.TabIndex = 29;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label10);
			this.groupBox1.Controls.Add(this.radioButtonExpencesAllTime);
			this.groupBox1.Controls.Add(this.dateTimePickerExpencesDateAndTime);
			this.groupBox1.Controls.Add(this.radioButtonExpencesOneDay);
			this.groupBox1.Controls.Add(this.label11);
			this.groupBox1.Controls.Add(this.radioButtonExpencesPeriod);
			this.groupBox1.Controls.Add(this.label12);
			this.groupBox1.Controls.Add(this.label23);
			this.groupBox1.Controls.Add(this.comboBoxExpencesSource);
			this.groupBox1.Controls.Add(this.label13);
			this.groupBox1.Controls.Add(this.buttonAddExpencesTransaction);
			this.groupBox1.Controls.Add(this.dateTimePickerExpencesOn);
			this.groupBox1.Controls.Add(this.buttonExpencesSourceEdit);
			this.groupBox1.Controls.Add(this.label24);
			this.groupBox1.Controls.Add(this.numericUpDownExpencesSumm);
			this.groupBox1.Controls.Add(this.dateTimePickerExpencesFrom);
			this.groupBox1.Controls.Add(this.buttonExpencesRemoveSelectedRows);
			this.groupBox1.Controls.Add(this.label25);
			this.groupBox1.Controls.Add(this.buttonExpencesDateCancel);
			this.groupBox1.Controls.Add(this.dateTimePickerExpencesTo);
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(696, 173);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(6, 18);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(98, 17);
			this.label10.TabIndex = 0;
			this.label10.Text = "Дата и время";
			// 
			// radioButtonExpencesAllTime
			// 
			this.radioButtonExpencesAllTime.AutoSize = true;
			this.radioButtonExpencesAllTime.Checked = true;
			this.radioButtonExpencesAllTime.Location = new System.Drawing.Point(6, 84);
			this.radioButtonExpencesAllTime.Name = "radioButtonExpencesAllTime";
			this.radioButtonExpencesAllTime.Size = new System.Drawing.Size(17, 16);
			this.radioButtonExpencesAllTime.TabIndex = 21;
			this.radioButtonExpencesAllTime.TabStop = true;
			this.radioButtonExpencesAllTime.UseVisualStyleBackColor = true;
			this.radioButtonExpencesAllTime.CheckedChanged += new System.EventHandler(this.radioButtonExpencesAllTime_CheckedChanged);
			// 
			// dateTimePickerExpencesDateAndTime
			// 
			this.dateTimePickerExpencesDateAndTime.CustomFormat = "d.MM.yyyy H:mm";
			this.dateTimePickerExpencesDateAndTime.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerExpencesDateAndTime.Location = new System.Drawing.Point(110, 17);
			this.dateTimePickerExpencesDateAndTime.Name = "dateTimePickerExpencesDateAndTime";
			this.dateTimePickerExpencesDateAndTime.Size = new System.Drawing.Size(107, 22);
			this.dateTimePickerExpencesDateAndTime.TabIndex = 1;
			// 
			// radioButtonExpencesOneDay
			// 
			this.radioButtonExpencesOneDay.AutoSize = true;
			this.radioButtonExpencesOneDay.Location = new System.Drawing.Point(6, 109);
			this.radioButtonExpencesOneDay.Name = "radioButtonExpencesOneDay";
			this.radioButtonExpencesOneDay.Size = new System.Drawing.Size(17, 16);
			this.radioButtonExpencesOneDay.TabIndex = 22;
			this.radioButtonExpencesOneDay.UseVisualStyleBackColor = true;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(260, 18);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(50, 17);
			this.label11.TabIndex = 2;
			this.label11.Text = "Сумма";
			// 
			// radioButtonExpencesPeriod
			// 
			this.radioButtonExpencesPeriod.AutoSize = true;
			this.radioButtonExpencesPeriod.Location = new System.Drawing.Point(6, 135);
			this.radioButtonExpencesPeriod.Name = "radioButtonExpencesPeriod";
			this.radioButtonExpencesPeriod.Size = new System.Drawing.Size(17, 16);
			this.radioButtonExpencesPeriod.TabIndex = 23;
			this.radioButtonExpencesPeriod.TabStop = true;
			this.radioButtonExpencesPeriod.UseVisualStyleBackColor = true;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(423, 18);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(71, 17);
			this.label12.TabIndex = 4;
			this.label12.Text = "Источник";
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(24, 84);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(91, 17);
			this.label23.TabIndex = 24;
			this.label23.Text = "Все расходы";
			// 
			// comboBoxExpencesSource
			// 
			this.comboBoxExpencesSource.FormattingEnabled = true;
			this.comboBoxExpencesSource.Location = new System.Drawing.Point(501, 17);
			this.comboBoxExpencesSource.Name = "comboBoxExpencesSource";
			this.comboBoxExpencesSource.Size = new System.Drawing.Size(121, 24);
			this.comboBoxExpencesSource.TabIndex = 5;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Enabled = false;
			this.label13.Location = new System.Drawing.Point(24, 108);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(83, 17);
			this.label13.TabIndex = 7;
			this.label13.Text = "Расходы за";
			// 
			// buttonAddExpencesTransaction
			// 
			this.buttonAddExpencesTransaction.Location = new System.Drawing.Point(462, 48);
			this.buttonAddExpencesTransaction.Name = "buttonAddExpencesTransaction";
			this.buttonAddExpencesTransaction.Size = new System.Drawing.Size(160, 23);
			this.buttonAddExpencesTransaction.TabIndex = 6;
			this.buttonAddExpencesTransaction.Text = "Добавить строку";
			this.buttonAddExpencesTransaction.UseVisualStyleBackColor = true;
			this.buttonAddExpencesTransaction.Click += new System.EventHandler(this.buttonAddExpencesTransaction_Click);
			// 
			// dateTimePickerExpencesOn
			// 
			this.dateTimePickerExpencesOn.CustomFormat = "d.MM.yyyy H:mm";
			this.dateTimePickerExpencesOn.Enabled = false;
			this.dateTimePickerExpencesOn.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerExpencesOn.Location = new System.Drawing.Point(113, 108);
			this.dateTimePickerExpencesOn.Name = "dateTimePickerExpencesOn";
			this.dateTimePickerExpencesOn.Size = new System.Drawing.Size(107, 22);
			this.dateTimePickerExpencesOn.TabIndex = 8;
			this.dateTimePickerExpencesOn.ValueChanged += new System.EventHandler(this.dateTimePickerExpencesOn_ValueChanged);
			// 
			// buttonExpencesSourceEdit
			// 
			this.buttonExpencesSourceEdit.BackgroundImage = global::FinansManager.Properties.Resources.EditButtonIco;
			this.buttonExpencesSourceEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.buttonExpencesSourceEdit.Location = new System.Drawing.Point(628, 16);
			this.buttonExpencesSourceEdit.Name = "buttonExpencesSourceEdit";
			this.buttonExpencesSourceEdit.Size = new System.Drawing.Size(61, 54);
			this.buttonExpencesSourceEdit.TabIndex = 10;
			this.buttonExpencesSourceEdit.UseVisualStyleBackColor = true;
			this.buttonExpencesSourceEdit.Click += new System.EventHandler(this.buttonExpencesSourceEdit_Click);
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Enabled = false;
			this.label24.Location = new System.Drawing.Point(24, 135);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(75, 17);
			this.label24.TabIndex = 25;
			this.label24.Text = "Расходы с";
			// 
			// numericUpDownExpencesSumm
			// 
			this.numericUpDownExpencesSumm.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
			this.numericUpDownExpencesSumm.Location = new System.Drawing.Point(316, 17);
			this.numericUpDownExpencesSumm.Name = "numericUpDownExpencesSumm";
			this.numericUpDownExpencesSumm.Size = new System.Drawing.Size(100, 22);
			this.numericUpDownExpencesSumm.TabIndex = 11;
			// 
			// dateTimePickerExpencesFrom
			// 
			this.dateTimePickerExpencesFrom.CustomFormat = "d.MM.yyyy H:mm";
			this.dateTimePickerExpencesFrom.Enabled = false;
			this.dateTimePickerExpencesFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerExpencesFrom.Location = new System.Drawing.Point(113, 135);
			this.dateTimePickerExpencesFrom.Name = "dateTimePickerExpencesFrom";
			this.dateTimePickerExpencesFrom.Size = new System.Drawing.Size(107, 22);
			this.dateTimePickerExpencesFrom.TabIndex = 26;
			this.dateTimePickerExpencesFrom.ValueChanged += new System.EventHandler(this.dateTimePickerExpences_ValueChanged);
			// 
			// buttonExpencesRemoveSelectedRows
			// 
			this.buttonExpencesRemoveSelectedRows.Location = new System.Drawing.Point(238, 48);
			this.buttonExpencesRemoveSelectedRows.Name = "buttonExpencesRemoveSelectedRows";
			this.buttonExpencesRemoveSelectedRows.Size = new System.Drawing.Size(169, 23);
			this.buttonExpencesRemoveSelectedRows.TabIndex = 12;
			this.buttonExpencesRemoveSelectedRows.Text = "Удалить выделение";
			this.buttonExpencesRemoveSelectedRows.UseVisualStyleBackColor = true;
			this.buttonExpencesRemoveSelectedRows.Click += new System.EventHandler(this.buttonExpencesRemoveSelectedRows_Click);
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Enabled = false;
			this.label25.Location = new System.Drawing.Point(226, 134);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(24, 17);
			this.label25.TabIndex = 27;
			this.label25.Text = "по";
			// 
			// buttonExpencesDateCancel
			// 
			this.buttonExpencesDateCancel.Location = new System.Drawing.Point(9, 47);
			this.buttonExpencesDateCancel.Name = "buttonExpencesDateCancel";
			this.buttonExpencesDateCancel.Size = new System.Drawing.Size(170, 23);
			this.buttonExpencesDateCancel.TabIndex = 13;
			this.buttonExpencesDateCancel.Text = "Сброс даты";
			this.buttonExpencesDateCancel.UseVisualStyleBackColor = true;
			this.buttonExpencesDateCancel.Click += new System.EventHandler(this.buttonExpencesDateCancel_Click);
			// 
			// dateTimePickerExpencesTo
			// 
			this.dateTimePickerExpencesTo.CustomFormat = "d.MM.yyyy H:mm";
			this.dateTimePickerExpencesTo.Enabled = false;
			this.dateTimePickerExpencesTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerExpencesTo.Location = new System.Drawing.Point(256, 135);
			this.dateTimePickerExpencesTo.Name = "dateTimePickerExpencesTo";
			this.dateTimePickerExpencesTo.Size = new System.Drawing.Size(109, 22);
			this.dateTimePickerExpencesTo.TabIndex = 28;
			this.dateTimePickerExpencesTo.ValueChanged += new System.EventHandler(this.dateTimePickerExpences_ValueChanged);
			// 
			// dataGridViewExpences
			// 
			this.dataGridViewExpences.AllowUserToAddRows = false;
			this.dataGridViewExpences.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dataGridViewExpences.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewExpences.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewExpences.Location = new System.Drawing.Point(3, 182);
			this.dataGridViewExpences.Name = "dataGridViewExpences";
			this.dataGridViewExpences.RowHeadersWidth = 51;
			this.dataGridViewExpences.RowTemplate.Height = 24;
			this.dataGridViewExpences.Size = new System.Drawing.Size(1431, 392);
			this.dataGridViewExpences.TabIndex = 9;
			// 
			// Statistic
			// 
			this.Statistic.Controls.Add(this.radioButtonStatisticsAllTime);
			this.Statistic.Controls.Add(this.radioButtonStatisticsOneDay);
			this.Statistic.Controls.Add(this.radioButtonStatisticsPeriod);
			this.Statistic.Controls.Add(this.label6);
			this.Statistic.Controls.Add(this.label7);
			this.Statistic.Controls.Add(this.dateTimePickerStatisticsOn);
			this.Statistic.Controls.Add(this.label8);
			this.Statistic.Controls.Add(this.dateTimePickerStatisticsTo);
			this.Statistic.Controls.Add(this.label16);
			this.Statistic.Controls.Add(this.dateTimePickerStatisticsFrom);
			this.Statistic.Controls.Add(this.textBoxStatistics);
			this.Statistic.Controls.Add(this.panel1);
			this.Statistic.Location = new System.Drawing.Point(4, 25);
			this.Statistic.Name = "Statistic";
			this.Statistic.Size = new System.Drawing.Size(1399, 577);
			this.Statistic.TabIndex = 3;
			this.Statistic.Text = "Статистика";
			this.Statistic.UseVisualStyleBackColor = true;
			// 
			// radioButtonStatisticsAllTime
			// 
			this.radioButtonStatisticsAllTime.AutoSize = true;
			this.radioButtonStatisticsAllTime.Checked = true;
			this.radioButtonStatisticsAllTime.Location = new System.Drawing.Point(4, 9);
			this.radioButtonStatisticsAllTime.Name = "radioButtonStatisticsAllTime";
			this.radioButtonStatisticsAllTime.Size = new System.Drawing.Size(17, 16);
			this.radioButtonStatisticsAllTime.TabIndex = 31;
			this.radioButtonStatisticsAllTime.TabStop = true;
			this.radioButtonStatisticsAllTime.UseVisualStyleBackColor = true;
			this.radioButtonStatisticsAllTime.CheckedChanged += new System.EventHandler(this.dateTimePickerStatiscics_ValueChanged);
			// 
			// radioButtonStatisticsOneDay
			// 
			this.radioButtonStatisticsOneDay.AutoSize = true;
			this.radioButtonStatisticsOneDay.Location = new System.Drawing.Point(4, 34);
			this.radioButtonStatisticsOneDay.Name = "radioButtonStatisticsOneDay";
			this.radioButtonStatisticsOneDay.Size = new System.Drawing.Size(17, 16);
			this.radioButtonStatisticsOneDay.TabIndex = 32;
			this.radioButtonStatisticsOneDay.UseVisualStyleBackColor = true;
			// 
			// radioButtonStatisticsPeriod
			// 
			this.radioButtonStatisticsPeriod.AutoSize = true;
			this.radioButtonStatisticsPeriod.Location = new System.Drawing.Point(4, 60);
			this.radioButtonStatisticsPeriod.Name = "radioButtonStatisticsPeriod";
			this.radioButtonStatisticsPeriod.Size = new System.Drawing.Size(17, 16);
			this.radioButtonStatisticsPeriod.TabIndex = 33;
			this.radioButtonStatisticsPeriod.TabStop = true;
			this.radioButtonStatisticsPeriod.UseVisualStyleBackColor = true;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(22, 9);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(91, 17);
			this.label6.TabIndex = 34;
			this.label6.Text = "Все расходы";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Enabled = false;
			this.label7.Location = new System.Drawing.Point(22, 33);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(83, 17);
			this.label7.TabIndex = 29;
			this.label7.Text = "Расходы за";
			// 
			// dateTimePickerStatisticsOn
			// 
			this.dateTimePickerStatisticsOn.CustomFormat = "d.MM.yyyy H:mm";
			this.dateTimePickerStatisticsOn.Enabled = false;
			this.dateTimePickerStatisticsOn.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerStatisticsOn.Location = new System.Drawing.Point(111, 33);
			this.dateTimePickerStatisticsOn.Name = "dateTimePickerStatisticsOn";
			this.dateTimePickerStatisticsOn.Size = new System.Drawing.Size(111, 22);
			this.dateTimePickerStatisticsOn.TabIndex = 30;
			this.dateTimePickerStatisticsOn.ValueChanged += new System.EventHandler(this.dateTimePickerStatiscics_ValueChanged);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Enabled = false;
			this.label8.Location = new System.Drawing.Point(22, 60);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(75, 17);
			this.label8.TabIndex = 35;
			this.label8.Text = "Расходы с";
			// 
			// dateTimePickerStatisticsTo
			// 
			this.dateTimePickerStatisticsTo.Enabled = false;
			this.dateTimePickerStatisticsTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerStatisticsTo.Location = new System.Drawing.Point(260, 60);
			this.dateTimePickerStatisticsTo.Name = "dateTimePickerStatisticsTo";
			this.dateTimePickerStatisticsTo.Size = new System.Drawing.Size(111, 22);
			this.dateTimePickerStatisticsTo.TabIndex = 4;
			this.dateTimePickerStatisticsTo.ValueChanged += new System.EventHandler(this.dateTimePickerStatiscics_ValueChanged);
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Enabled = false;
			this.label16.Location = new System.Drawing.Point(228, 60);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(24, 17);
			this.label16.TabIndex = 3;
			this.label16.Text = "по";
			// 
			// dateTimePickerStatisticsFrom
			// 
			this.dateTimePickerStatisticsFrom.Enabled = false;
			this.dateTimePickerStatisticsFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimePickerStatisticsFrom.Location = new System.Drawing.Point(111, 60);
			this.dateTimePickerStatisticsFrom.Name = "dateTimePickerStatisticsFrom";
			this.dateTimePickerStatisticsFrom.Size = new System.Drawing.Size(111, 22);
			this.dateTimePickerStatisticsFrom.TabIndex = 2;
			this.dateTimePickerStatisticsFrom.ValueChanged += new System.EventHandler(this.dateTimePickerStatiscics_ValueChanged);
			// 
			// textBoxStatistics
			// 
			this.textBoxStatistics.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.textBoxStatistics.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBoxStatistics.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxStatistics.Font = new System.Drawing.Font("Fira Code", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxStatistics.Location = new System.Drawing.Point(0, 89);
			this.textBoxStatistics.Multiline = true;
			this.textBoxStatistics.Name = "textBoxStatistics";
			this.textBoxStatistics.ReadOnly = true;
			this.textBoxStatistics.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
			this.textBoxStatistics.Size = new System.Drawing.Size(1399, 488);
			this.textBoxStatistics.TabIndex = 9;
			this.textBoxStatistics.WordWrap = false;
			// 
			// panel1
			// 
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1399, 89);
			this.panel1.TabIndex = 36;
			// 
			// FormMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1407, 606);
			this.Controls.Add(this.tabControl1);
			this.MinimumSize = new System.Drawing.Size(726, 238);
			this.Name = "FormMain";
			this.Text = "Финансовый менеджер";
			this.Load += new System.EventHandler(this.FormMain_Load);
			this.tabControl1.ResumeLayout(false);
			this.Balance.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chartBalance)).EndInit();
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.tableLayoutPanel3.ResumeLayout(false);
			this.tableLayoutPanel3.PerformLayout();
			this.Revenue.ResumeLayout(false);
			this.tableLayoutPanel10.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownRevenueSumm)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewRevenue)).EndInit();
			this.Expenses.ResumeLayout(false);
			this.tableLayoutPanel9.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownExpencesSumm)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewExpences)).EndInit();
			this.Statistic.ResumeLayout(false);
			this.Statistic.PerformLayout();
			this.ResumeLayout(false);

		}


		#endregion

		public System.Windows.Forms.TabControl tabControl1;
		public System.Windows.Forms.TabPage Balance;
		public System.Windows.Forms.TextBox textBoxBalanceRevenue;
		public System.Windows.Forms.Label label3;
		public System.Windows.Forms.DataVisualization.Charting.Chart chartBalance;
		public System.Windows.Forms.DateTimePicker dateTimePickerBalanceTo;
		public System.Windows.Forms.Label label2;
		public System.Windows.Forms.DateTimePicker dateTimePickerBalanceFrom;
		public System.Windows.Forms.Label label1;
		public System.Windows.Forms.TabPage Revenue;
		public System.Windows.Forms.TabPage Expenses;
		public System.Windows.Forms.TabPage Statistic;
		public System.Windows.Forms.TextBox textBoxBalanceBalance;
		public System.Windows.Forms.Label label5;
		public System.Windows.Forms.TextBox textBoxBalanceExpences;
		public System.Windows.Forms.Label label4;
		public System.Windows.Forms.DateTimePicker dateTimePickerExpencesDateAndTime;
		public System.Windows.Forms.Label label10;
		public System.Windows.Forms.DateTimePicker dateTimePickerExpencesOn;
		public System.Windows.Forms.Label label13;
		public System.Windows.Forms.Button buttonAddExpencesTransaction;
		public System.Windows.Forms.ComboBox comboBoxExpencesSource;
		public System.Windows.Forms.Label label12;
		public System.Windows.Forms.Label label11;
		public System.Windows.Forms.DateTimePicker dateTimePickerStatisticsTo;
		public System.Windows.Forms.Label label16;
		public System.Windows.Forms.DateTimePicker dateTimePickerStatisticsFrom;
		private System.Windows.Forms.Button buttonExpencesSourceEdit;
		public System.Windows.Forms.NumericUpDown numericUpDownExpencesSumm;
		private System.Windows.Forms.Button buttonExpencesDateCancel;
		private System.Windows.Forms.Button buttonExpencesRemoveSelectedRows;
		public System.Windows.Forms.Label label24;
		public System.Windows.Forms.DateTimePicker dateTimePickerExpencesFrom;
		public System.Windows.Forms.Label label25;
		public System.Windows.Forms.DateTimePicker dateTimePickerExpencesTo;
		public System.Windows.Forms.DataGridView dataGridViewExpences;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
		private System.Windows.Forms.GroupBox groupBox2;
		public System.Windows.Forms.Label label26;
		public System.Windows.Forms.DateTimePicker dateTimePickerRevenueDateAndTime;
		public System.Windows.Forms.Label label27;
		public System.Windows.Forms.Label label28;
		public System.Windows.Forms.ComboBox comboBoxRevenueSource;
		public System.Windows.Forms.Label label9;
		public System.Windows.Forms.Button button2;
		public System.Windows.Forms.DateTimePicker dateTimePickerRevenueOn;
		private System.Windows.Forms.Button button3;
		public System.Windows.Forms.Label label20;
		public System.Windows.Forms.NumericUpDown numericUpDownRevenueSumm;
		public System.Windows.Forms.DateTimePicker dateTimePickerRevenueFrom;
		private System.Windows.Forms.Button button4;
		public System.Windows.Forms.Label label21;
		private System.Windows.Forms.Button button5;
		public System.Windows.Forms.DateTimePicker dateTimePickerRevenueTo;
		public System.Windows.Forms.DataGridView dataGridViewRevenue;
		public System.Windows.Forms.TextBox textBoxStatistics;
		public System.Windows.Forms.Label label7;
		public System.Windows.Forms.DateTimePicker dateTimePickerStatisticsOn;
		public System.Windows.Forms.Label label8;
		public System.Windows.Forms.RadioButton radioButtonStatisticsAllTime;
		public System.Windows.Forms.RadioButton radioButtonStatisticsOneDay;
		public System.Windows.Forms.RadioButton radioButtonStatisticsPeriod;
		public System.Windows.Forms.Label label6;
		public System.Windows.Forms.RadioButton radioButtonExpencesAllTime;
		public System.Windows.Forms.RadioButton radioButtonExpencesOneDay;
		public System.Windows.Forms.RadioButton radioButtonExpencesPeriod;
		public System.Windows.Forms.Label label23;
		public System.Windows.Forms.RadioButton radioButtonRevenueAllTime;
		public System.Windows.Forms.RadioButton radioButtonRevenueOneDay;
		public System.Windows.Forms.RadioButton radioButtonRevenuePeriod;
		public System.Windows.Forms.Label label22;
		private System.Windows.Forms.Panel panel1;
	}
}

