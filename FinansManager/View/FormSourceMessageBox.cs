﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FinansManager.View
{
	public partial class FormSourceMessageBox : Form
	{
		public FormSourceMessageBox()
		{
			InitializeComponent();
		}
		private void buttonDelete_Click(object sender, EventArgs e)
		{
				DialogResult = DialogResult.Yes;
			Close();
		}

		private void buttonSkip_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
			Close();
		}

		public DialogResult Show(string message)
		{
			label1.Text = message;
			return ShowDialog();
		}
	}
}
