﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinansManager
{
	public partial class FormMain : Form
	{
		private ControllerFormMain controller;
		public FormMain()
		{
			InitializeComponent();
		}

		private void FormMain_Load(object sender, EventArgs e)
		{
			controller = new ControllerFormMain(this);
			controller.ControlsInitialize();
		}

		#region Revenues
		private void buttonRevenueSourceEdit_Click(object sender, EventArgs e)
		{
			controller.OpenFormSource(ER.Revenues);
		}
		private void buttonAddRevenueTransaction_Click(object sender, EventArgs e)
		{
			controller.AddNewRevenueOrExpences(ER.Revenues);
		}
		private void dateTimePickerRevenueOn_ValueChanged(object sender, EventArgs e)
		{
			controller.FillDataGridView(dataGridViewRevenue, dateTimePickerRevenueOn.Value);
		}
		private void buttonRevenueDateCancel_Click(object sender, EventArgs e)
		{
			controller.ResetRevenueDateTimePickers();
		}
		private void buttonRevenueRemoveSelectedRows_Click(object sender, EventArgs e)
		{
			controller.DeleteSelectedER(ER.Revenues);
		}
		private void radioButton1_CheckedChanged(object sender, EventArgs e)
		{
			controller.FillDataGridView(dataGridViewRevenue);
		}
		private void dateTimePickerRevenue_ValueChanged(object sender, EventArgs e)
		{
				controller.FillDataGridView(dataGridViewRevenue, dateTimePickerRevenueFrom.Value, dateTimePickerRevenueTo.Value);
		}

		#endregion
		#region Expences
		private void dateTimePickerExpencesOn_ValueChanged(object sender, EventArgs e)
		{
			controller.FillDataGridView(dataGridViewExpences);
		}
		private void buttonAddExpencesTransaction_Click(object sender, EventArgs e)
		{
			controller.AddNewRevenueOrExpences(ER.Excpences);
		}
		private void buttonExpencesRemoveSelectedRows_Click(object sender, EventArgs e)
		{
			controller.DeleteSelectedER(ER.Excpences);
		}
		private void buttonExpencesDateCancel_Click(object sender, EventArgs e)
		{
			controller.ResetExpencesDateTimePickers();
		}
		private void buttonExpencesSourceEdit_Click(object sender, EventArgs e)
		{
			controller.OpenFormSource(ER.Excpences);
		}


		private void radioButtonExpencesAllTime_CheckedChanged(object sender, EventArgs e)
		{
			controller.FillDataGridView(dataGridViewExpences);
		}
		private void dateTimePickerExpences_ValueChanged(object sender, EventArgs e)
		{
			controller.FillDataGridView(dataGridViewExpences, dateTimePickerExpencesFrom.Value, dateTimePickerExpencesTo.Value);
		}
		#endregion

		private void dateTimePickerBalance_ValueChanged(object sender, EventArgs e)
		{
			controller.FillBalanceFills();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			controller.DbManagerRandomFill();
		}
		private void dateTimePickerStatiscics_ValueChanged(object sender, EventArgs e)
		{
			controller.FillStatictic();
		}
	}
}
